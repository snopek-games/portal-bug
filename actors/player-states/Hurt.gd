extends "res://addons/snopek_state_machine/State.gd"

onready var host = $"../.."

func _state_enter(info: Dictionary) -> void:
	host.animation_player.play("Hurt")
	host.sounds.play("Hurt")
	var push_back_vector = info['push_back_vector'] if info.has("push_back_vector") else Vector2.UP
	host.vector = push_back_vector * host.push_back_speed

func _on_AnimationPlayer_animation_finished(anim_name: String) -> void:
	if anim_name == "Hurt" and host.state_machine.current_state == self:
		if host.invincible:
			host.state_machine.change_state("Idle")
		else:
			host.state_machine.change_state("Dead")
