extends "res://actors/player-states/Move.gd"

var biting := false
var bite_hitbox

func _state_enter(info: Dictionary) -> void:
	host.animation_player.play("Dive")
	host.sounds.play("Dive")
	biting = false
	bite_hitbox = null

func _state_exit() -> void:
	if bite_hitbox:
		bite_hitbox.disabled = true

func _state_physics_process(delta: float) -> void:
	if host.is_on_floor():
		get_parent().change_state("Land")
		return
	
	if !biting and Input.is_action_pressed("player_attack"):
		biting = true
		host.animation_player.play("DiveBite")
		host.sounds.play("Bite")
		bite_hitbox = host.hitbox_dive_bite_left if host.sprite.flip_h else host.hitbox_dive_bite_right
		bite_hitbox.disabled = false
	if !biting and Input.is_action_just_released("player_down"):
		get_parent().change_state("Fall")
		return
	
	var input_vector = _get_player_input_vector()
	do_flip_sprite(input_vector)
	
	# Point the input vector down.
	input_vector.y = 1.0
	# Don't normalize this! It feels better when the diagonal moves a little
	# bit further.
	#input_vector = input_vector.normalized()
	
	# Accelerate towards top speed.
	host.vector = host.vector.move_toward(input_vector * host.dive_speed, host.dive_acceleration * delta)

func _on_AnimationPlayer_animation_finished(anim_name: String) -> void:
	if anim_name == 'DiveBite' and get_parent().current_state == self:
		if Input.is_action_pressed("player_down"):
			host.animation_player.play("Dive")
			biting = false
			bite_hitbox.disabled = true
			bite_hitbox = null
		else:
			get_parent().change_state("Fall")
			return

