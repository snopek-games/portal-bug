extends "res://actors/player-states/Move.gd"

var glide_sound = null

func _state_enter(info: Dictionary) -> void:
	host.animation_player.play("Fall")

func _state_exit() -> void:
	if glide_sound:
		glide_sound.stop()

func _state_physics_process(delta: float) -> void:
	if host.is_on_floor():
		get_parent().change_state("Land")
		return
	
	var input_vector = _get_player_input_vector()
	do_move(input_vector)
	
	# If the player presses down, then we dive.
	if Input.is_action_just_pressed("player_down"):
		get_parent().change_state("Dive")
		return
	
	# Allow the character to "stall" their fall by holding jump.
	if Input.is_action_pressed("player_jump"):
		host.vector.y = 10.0
		host.animation_player.play("Glide")
		if glide_sound == null or !glide_sound.playing:
			glide_sound = host.sounds.play("Glide")
	elif host.animation_player.current_animation != "Fall":
		host.animation_player.play("Fall")
		if glide_sound and glide_sound.playing:
			glide_sound.stop()
	
