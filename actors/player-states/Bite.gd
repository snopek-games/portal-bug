extends "res://addons/snopek_state_machine/State.gd"

onready var host = $"../.."

var bite_hitbox

func _state_enter(info: Dictionary) -> void:
	host.animation_player.play("Bite")
	host.sounds.play("Bite")
	
	var bite_direction = -1 if host.sprite.flip_h else 1
	host.vector.x = bite_direction * host.bite_speed
	
	bite_hitbox = host.hitbox_bite_left if host.sprite.flip_h else host.hitbox_bite_right
	bite_hitbox.disabled = false
	
	

func _state_exit() -> void:
	bite_hitbox.disabled = true

func _on_AnimationPlayer_animation_finished(anim_name: String) -> void:
	if anim_name == 'Bite' and get_parent().current_state == self:
		get_parent().change_state("Idle")
