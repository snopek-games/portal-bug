extends "res://actors/player-states/Move.gd"

onready var fly_timer = $FlyTimer

var flying_sound = null

func _state_enter(info: Dictionary) -> void:
	host.animation_player.play("Fly")
	flying_sound = host.sounds.play("Fly")
	host.vector.y = -(host.jump_speed * 0.5)
	#host.vector.y = 0.0
	
	if info.has('input_vector'):
		do_move(info['input_vector'])
	
	fly_timer.start()

func _state_exit() -> void:
	fly_timer.stop()
	if flying_sound:
		flying_sound.stop()

func _state_physics_process(delta: float) -> void:
	var input_vector = _get_player_input_vector()
	do_move(input_vector)
	
	# If the player presses down, then we dive.
	if Input.is_action_just_pressed("player_down"):
		get_parent().change_state("Dive")
		return
	
	# If the player releases the jump key, then interrupt the flying.
	if Input.is_action_just_released("player_jump"):
		host.vector.y = 0.0
		get_parent().change_state("Fall")
		return
	
	# Slowly ascend.
	host.vector.y -= (host.fly_speed * delta)

func _on_FlyTimer_timeout() -> void:
	host.vector.y = 0.0
	get_parent().change_state("Fall")
