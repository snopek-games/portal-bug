extends "res://addons/snopek_state_machine/State.gd"

onready var host = $"../.."

func _ready() -> void:
	MapLoader.connect("map_loaded", self, "_on_map_loaded")

func _state_enter(info: Dictionary) -> void:
	host.animation_player.play("Teleport")
	host.sounds.play("Portal")

func _state_physics_process(delta: float) -> void:
	host.vector.y = -1.0
	
	# Decelerate to 0.
	if host.vector.x < 0:
		host.vector.x = min(0.0, host.vector.x + (host.friction * delta))
	elif host.vector.x > 0:
		host.vector.x = max(0.0, host.vector.x - (host.friction * delta))

func _on_map_loaded() -> void:
	if get_parent().current_state == self:
		host.animation_player.play_backwards("Teleport")
		host.sounds.play("PortalReverse")
		yield(host.animation_player, "animation_finished")
		get_parent().change_state("Idle")
