extends "res://addons/snopek_state_machine/State.gd"

onready var host = $"../.."

var vector = Vector2.ZERO

func _state_enter(info: Dictionary) -> void:
	host.animation_player.play("Walk")
	host.sprite.flip_h = false
	vector = Vector2.RIGHT * host.walk_speed

func _state_physics_process(delta: float) -> void:
	var collision = host.move_and_collide(vector * delta)
	if collision:
		reverse_direction()
	else:
		if !host.raycast1.is_colliding() or !host.raycast2.is_colliding():
			reverse_direction()

func reverse_direction() -> void:
	vector = vector * Vector2(-1, 0)
	host.sprite.flip_h = !host.sprite.flip_h
	
	# Move in the new direction right away to stop collision.
	var delta = get_physics_process_delta_time()
	host.move_and_collide(vector * 2.0 * delta)
