extends KinematicBody2D

export (float) var walk_speed = 100.0

onready var sprite := $Sprite
onready var animation_player := $AnimationPlayer
onready var state_machine := $StateMachine
onready var raycast1 := $RayCast1
onready var raycast2 := $RayCast2
onready var hurt_sound := $HurtSound

onready var original_position := global_position

func _ready() -> void:
	state_machine.change_state("Walk")

func hurt(node) -> void:
	state_machine.change_state("Dead")
