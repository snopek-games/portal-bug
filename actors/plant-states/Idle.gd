extends "res://addons/snopek_state_machine/State.gd"

onready var host := $"../.."
onready var attack_timer := $AttackTimer

var player_in_attack_zone := false

func _state_enter(info: Dictionary) -> void:
	host.animation_player.play("Idle")

func _state_exit() -> void:
	attack_timer.stop()

func _state_physics_process(delta: float) -> void:
	var player = MapLoader._player
	var new_flip_h: bool
	if player.global_position.x > host.global_position.x:
		new_flip_h = true
	else:
		new_flip_h = false
	if host.sprite.flip_h != new_flip_h:
		host.sprite.flip_h = new_flip_h
		host.attack_zone_left_shape.set_deferred("disabled", new_flip_h)
		host.attack_zone_right_shape.set_deferred("disabled", !new_flip_h)
	
	# If the player hasn't left the attack zone, we need to make sure to start
	# the attack timer again.
	if player_in_attack_zone and attack_timer.is_stopped():
		attack_timer.start()
	
func _on_AttackZone_body_entered(body: Node) -> void:
	player_in_attack_zone = true
	if get_parent().current_state == self:
		attack_timer.start()

func _on_AttackZone_body_exited(body: Node) -> void:
	player_in_attack_zone = false
	attack_timer.stop()

func _on_AttackTimer_timeout() -> void:
	if get_parent().current_state == self:
		get_parent().change_state("Attack")
