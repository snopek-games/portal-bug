extends "res://addons/snopek_state_machine/State.gd"

onready var host := $"../.."

func _state_enter(info: Dictionary) -> void:
	host.animation_player.play("Attack")

func _on_AnimationPlayer_animation_finished(anim_name: String) -> void:
	if anim_name == "Attack" and get_parent().current_state == self:
		get_parent().change_state("Idle")

func do_attack() -> void:
	$AttackSound.play()
	var colliding_bodies := []
	colliding_bodies += host.attack_zone_left.get_overlapping_bodies()
	colliding_bodies += host.attack_zone_right.get_overlapping_bodies()
	
	if colliding_bodies.size() > 0:
		var colliding_body = colliding_bodies[0]
		if colliding_body.has_method("hurt"):
			colliding_body.hurt(host)
