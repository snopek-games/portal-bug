extends "res://addons/snopek_state_machine/State.gd"

onready var host = $"../.."

func _state_enter(info: Dictionary) -> void:
	host.animation_player.play("Dead")

func _on_AnimationPlayer_animation_finished(anim_name: String) -> void:
	if host.state_machine.current_state != self:
		return
	if anim_name == "Dead":
		$RespawnTimer.start()
	elif anim_name == "Respawn":
		get_parent().change_state("Idle")

func _on_RespawnTimer_timeout() -> void:
	host.animation_player.play("Respawn")
