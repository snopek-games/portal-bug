extends KinematicBody2D

onready var sprite := $Sprite
onready var animation_player := $AnimationPlayer
onready var state_machine := $StateMachine

onready var attack_zone_left := $AttackZoneLeft
onready var attack_zone_left_shape := $AttackZoneLeft/CollisionShape2D
onready var attack_zone_right := $AttackZoneRight
onready var attack_zone_right_shape := $AttackZoneRight/CollisionShape2D

func _ready() -> void:
	state_machine.change_state("Idle")

func hurt(node) -> void:
	# DRS: Make the plants invincible?
	#state_machine.change_state("Dead")
	pass
