extends KinematicBody2D
class_name Player

export (float) var speed := 400.0
export (float) var acceleration := 2000.0
export (float) var friction := 4000.0
export (float) var gravity := 800.0
export (float) var jump_speed := 400.0
export (float) var fly_speed := 700.0
export (float) var dive_speed := 800.0
export (float) var dive_acceleration := 40000.0
export (float) var terminal_velocity := 1600.0
export (float) var push_back_speed := 400.0
export (float) var bite_speed := 100.0
export (bool) var invincible := false

signal player_dead ()

onready var sprite := $Sprite
onready var animation_player := $AnimationPlayer
onready var state_machine := $StateMachine
onready var sounds := $Sounds

onready var hitbox_bite_right := $HitboxBiteRight
onready var hitbox_bite_left := $HitboxBiteLeft
onready var hitbox_dive_bite_right := $HitboxDiveBiteRight
onready var hitbox_dive_bite_left := $HitboxDiveBiteLeft

var vector := Vector2.ZERO

func reset_state() -> void:
	var current_state_name = state_machine.current_state.name if state_machine.current_state != null else "None"
	if current_state_name != "Idle" and current_state_name != "Teleport":
		state_machine.change_state("Idle")
	sprite.flip_h = false
	visible = true

func hurt(node: Node2D) -> void:
	var current_state_name = state_machine.current_state.name if state_machine.current_state != null else "None"
	if current_state_name == "Hurt" or current_state_name == "Dead" or current_state_name == "Bite" or current_state_name == "DiveBite" or current_state_name == "Teleport":
		return
	
	var push_back_vector = (global_position - node.global_position).normalized()
	if node.is_in_group('plant'):
		visible = false
		push_back_vector = Vector2.ZERO
		
	state_machine.change_state("Hurt", {
		push_back_vector = push_back_vector,
	})

func _ready() -> void:
	reset_state()

func _physics_process(delta: float) -> void:
	vector.y += (gravity * delta)
	if vector.y > terminal_velocity:
		vector.y = terminal_velocity
	vector = move_and_slide(vector, Vector2.UP)
	
	if position.y > 96:
		state_machine.change_state("Dead")
	
