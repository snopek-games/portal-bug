extends Control

signal fade_out_finished
signal fade_in_finished

onready var animation_player = $AnimationPlayer

func fade_out():
	animation_player.play("FadeOut")

func fade_in():
	animation_player.play("FadeIn")

func _on_AnimationPlayer_animation_finished(anim_name: String) -> void:
	if anim_name == 'FadeOut':
		emit_signal("fade_out_finished")
	elif anim_name == 'FadeIn':
		emit_signal("fade_in_finished")
