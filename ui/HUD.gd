extends Control

onready var collectibles_count_label = $CollectiblesContainer/CollectiblesCountLabel

func _ready():
	GameState.connect("collectibles_count_changed", self, "_on_GameState_collectibles_count_changed")

func _on_GameState_collectibles_count_changed(count: int) -> void:
	collectibles_count_label.text = str(count)
