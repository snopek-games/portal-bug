extends Area2D

export (bool) var disabled := false setget set_disabled

func set_disabled(_disabled: bool) -> void:
	if disabled != _disabled:
		disabled = _disabled
		for child in get_children():
			if child is CollisionShape2D or child is CollisionPolygon2D:
				child.set_deferred("disabled", disabled)

func _on_body_entered(body: Node) -> void:
	if body.has_method('hurt'):
		body.hurt(get_parent())
