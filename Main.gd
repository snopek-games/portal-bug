extends Node2D
class_name Main

export (PackedScene) var main_map_scene

onready var music = $Music
onready var fade = $OverlayLayer/Fade
onready var player = $Player
onready var camera = $Player/Camera2D
onready var win_screen = $OverlayLayer/WinScreen
onready var win_timer = $WinTimer

func _ready() -> void:
	VisualServer.set_default_clear_color(Color(1.0, 1.0, 1.0, 1.0))
	GameState.connect("win", self, "_on_GameState_win")
	MapLoader.setup(self, player, main_map_scene)
	music.play("GameMusic")

func _on_Player_player_dead() -> void:
	music.play("LoseMusic")
	fade.fade_out()
	yield(fade, "fade_out_finished")
	yield(music, "song_finished")
	
	player.reset_state()
	# TODO: How to reset the map?
	
	player.global_position = GameState.current_checkpoint.global_position
	camera.global_position = player.global_position
	camera.smoothing_enabled = false
	camera.call_deferred("reset_smoothing")
	camera.set_deferred("smoothing_enabled", true)
	
	fade.fade_in()
	music.play("GameMusic")

func _on_GameState_win() -> void:
	music.play("WinMusic")
	player.state_machine.change_state("Teleport")
	win_screen.visible = true
	win_timer.start()

func _on_WinTimer_timeout() -> void:
	win_screen.visible = false
	MapLoader.load_map(main_map_scene)
	GameState.reset()
	yield(fade, "fade_in_finished")
	music.play("GameMusic")
