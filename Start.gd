extends Control

func _ready() -> void:
	if OS.get_name() != "HTML5":
		start_game()

func _on_PlayButton_pressed() -> void:
	start_game()

func start_game() -> void:
	get_tree().change_scene("res://Main.tscn")
	
