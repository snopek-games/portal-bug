extends Area2D

export (bool) var checkpoint_activated = false setget set_checkpoint_activated

func _ready() -> void:
	GameState.connect("checkpoint_changed", self, "_on_GameState_checkpoint_changed")

func set_checkpoint_activated(_checkpoint_activated: bool) -> void:
	if checkpoint_activated != _checkpoint_activated:
		checkpoint_activated = _checkpoint_activated
		$Sprite.frame = 1 if checkpoint_activated else 0

func _on_GameState_checkpoint_changed(checkpoint) -> void:
	if checkpoint != self:
		set_checkpoint_activated(false)

func _on_Checkpoint_body_entered(body: Node) -> void:
	if not checkpoint_activated:
		GameState.current_checkpoint = self
		set_checkpoint_activated(true)
