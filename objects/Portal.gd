extends Area2D

export (PackedScene) var new_scene

func _on_Portal_body_entered(body: Node) -> void:
	var state_machine = body.get('state_machine')
	if state_machine:
		state_machine.change_state('Teleport')
	MapLoader.load_map(new_scene)
