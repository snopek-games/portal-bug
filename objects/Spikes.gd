extends StaticBody2D

func _on_Hurtbox_body_entered(body: Node) -> void:
	if body.has_method('hurt'):
		body.hurt(self)
	
