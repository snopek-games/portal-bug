extends Area2D

onready var animation_player := $AnimationPlayer

func _on_Collectable_body_entered(body: Node) -> void:
	GameState.collectibles_count += 1
	$AudioStreamPlayer.play()
	animation_player.play("Collect")
	yield(animation_player, "animation_finished")
	queue_free()
