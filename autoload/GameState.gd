extends Node

signal checkpoint_changed (checkpoint)
signal collectibles_count_changed (count)
signal win ()

var current_checkpoint: Node2D = null setget set_current_checkpoint
var collectibles_count: int = 0 setget set_collectibles_count

func set_current_checkpoint(_current_checkpoint: Node2D) -> void:
	if current_checkpoint != _current_checkpoint:
		current_checkpoint = _current_checkpoint
		emit_signal("checkpoint_changed", current_checkpoint)

func set_collectibles_count(_collectibles_count: int) -> void:
	if collectibles_count != _collectibles_count:
		collectibles_count = _collectibles_count
		emit_signal("collectibles_count_changed", collectibles_count)

func reset() -> void:
	self.collectibles_count = 0

func win() -> void:
	emit_signal("win")
