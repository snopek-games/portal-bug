extends Node

signal map_loaded ()

var _game: Main = null
var _player: Player = null
var _map: Node2D = null

onready var scene_tree := get_tree()

func setup(game: Main, player: Player, level: PackedScene) -> void:
	_game = game
	_player = player
	load_map(level)

func load_map(map_scene: PackedScene) -> void:
	if _map:
		_game.fade.fade_out()
		yield(_game.fade, "fade_out_finished")
		remove_child(_map)
		_map.queue_free()
		
	_map = map_scene.instance()
	_map.name = 'Map'
	_map.pause_mode = Node.PAUSE_MODE_STOP
	_game.add_child(_map)
	_game.move_child(_map, 0)
	
	var player_start_position = _map.get_node('PlayerStartPosition')
	if player_start_position:
		_player.global_position = player_start_position.global_position
		GameState.current_checkpoint = player_start_position
	else:
		_player.global_position = Vector2(64 * 4, 64 * 4)
		GameState.current_checkpoint = null
	
	_player.reset_state()
	_game.camera.global_position = _player.global_position
	_game.camera.smoothing_enabled = false
	_game.camera.call_deferred("reset_smoothing")
	_game.camera.set_deferred("smoothing_enabled", true)
	
	_game.fade.fade_in()
	yield(_game.fade, "fade_in_finished")
	emit_signal("map_loaded")
